package lygo_ext_themifly

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_vfs/commons"
)

// ---------------------------------------------------------------------------------------------------------------------
//	ThemiflySettings
// ---------------------------------------------------------------------------------------------------------------------

type ThemiflySettings struct {
	TargetDir *ThemiflySettingsTarget   `json:"target-dir"`
	Blog      *ThemiflyBlogSettings  `json:"blog"`
	SiteData  map[string]interface{} `json:"site-data"`
}

type ThemiflySettingsTarget struct {
	*commons.VfsSettings
	Home string `json:"home"`
}

type ThemiflyBlogSettings struct {
	PostPerPage      int  `json:"post-per-page"`
	KeepUploadedPost bool `json:"keep-uploaded-post"`
}

func LoadSettings(filename string) (*ThemiflySettings, error) {
	if b, _ := lygo_paths.Exists(filename); !b {
		_, _ = lygo_io.WriteTextToFile(SettingsJson, filename)
	}

	var instance ThemiflySettings
	// load settings
	settings, err := lygo_io.ReadTextFromFile(filename)
	if nil != err {
		return nil, err
	}
	err = lygo_json.Read(settings, &instance)
	if nil != err {
		return nil, err
	}

	return &instance, err
}
