package lygo_ext_themifly

import (
	"bitbucket.org/lygo/lygo_commons/lygo_crypto"
	"bitbucket.org/lygo/lygo_commons/lygo_date"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_events"
	"bitbucket.org/lygo/lygo_ext_vfs/commons"
	"fmt"
	"strings"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//	ThemiFly
// ---------------------------------------------------------------------------------------------------------------------

type ThemiFly struct {
	dirWork     string
	dirTemplate string
	dirUploads  string
	dirBlocks   string
	dirBlog     string

	running  bool
	stopChan chan bool
	settings *ThemiflySettings
	ticker   *lygo_events.EventTicker
	indexer  *Indexer
	vfs      commons.IVfs
}

func NewThemiFly(path string) (*ThemiFly, error) {
	instance := new(ThemiFly)
	err := instance.init(path)

	return instance, err
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ThemiFly) Open() {
	if nil != instance && nil != instance.ticker && !instance.running {
		// index template
		instance.indexer.Run()

		// check for new uploads
		instance.checkUploads()
		instance.ticker.Start()

		instance.stopChan = make(chan bool, 1)

		instance.running = true
	}
}

func (instance *ThemiFly) Close() {
	if nil != instance && nil != instance.ticker && instance.running {
		instance.ticker.Stop()

		instance.stopChan <- true
		instance.stopChan = nil

		instance.running = false
	}
}

func (instance *ThemiFly) Wait() {
	if nil != instance && instance.running {
		// wait exit
		<-instance.stopChan
		// reset channel
		instance.stopChan = make(chan bool, 1)
	}
}

func (instance *ThemiFly) RunIndexer() error {
	if nil != instance {
		// index template
		return instance.indexer.Run()
	}
	return nil
}

func (instance *ThemiFly) Reload() error {
	if nil != instance {
		// index template
		err := instance.indexer.Run()
		if nil != err {
			return err
		}
		// check for new uploads
		if instance.running {
			instance.ticker.Pause()
			instance.checkUploads()
			instance.ticker.Resume()
		}
	}
	return nil
}

// site builder
func (instance *ThemiFly) Build() (map[string]interface{}, error) {
	if nil != instance {
		blocks := instance.indexer.blockItems
		template := instance.indexer.templateItems
		if len(blocks) > 0 && len(template) > 0 {
			deployer := NewThemiFlyBuilder(instance.dirWork, blocks, template, instance.settings, instance.vfs)
			return deployer.Run()
		}
	}
	return nil, nil
}

// publish a blog post
func (instance *ThemiFly) Publish(meta interface{}, content string) (err error) {
	if nil != instance {
		// create the blogger
		blogger, e := NewThemiFlyBlogger(instance.dirWork, instance.dirBlog,
			instance.indexer.blockItems, instance.settings, instance.vfs)
		if nil != e {
			err = e
			return
		}
		// ready to publish
		err = blogger.Publish(meta, content)

	}
	return
}

// publish a blog post from dir
func (instance *ThemiFly) PublishDir(dir string) (err error) {
	if nil != instance {
		m := instance.mapFiles(dir)
		if nil != m {
			for _, v := range m {
				instance.publish(v)
			}
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	u t i l i t y
// ---------------------------------------------------------------------------------------------------------------------

// reset all blog data (counter, tags, etc...)
func (instance *ThemiFly) ResetBlogData() error {
	if nil != instance {
		// index template
		blogger, e := NewThemiFlyBlogger(instance.dirWork, instance.dirBlog,
			instance.indexer.blockItems, instance.settings, instance.vfs)
		if nil != e {
			return e
		}
		return blogger.ResetData()
	}
	return nil
}

func (instance *ThemiFly) GenerateBlogPostStub(name, target string) (err error) {
	target = lygo_paths.Concat(instance.dirWork, target)
	if len(name) == 0 {
		name = "post"
	}

	// image
	image := lygo_paths.Concat(target, fmt.Sprintf("%v.png", name))
	bytes, _ := lygo_crypto.DecodeBase64(strings.Replace(ImagePost, "data:image/jpeg;base64,", "", 1))
	_, err = lygo_io.WriteBytesToFile(bytes, image)
	if nil != err {
		return err
	}

	// post
	post := lygo_paths.Concat(target, fmt.Sprintf("%v.html", name))
	_, err = lygo_io.WriteTextToFile("<p>Here goes you content. <br>Write any HTML text with images or any thing you need</p>", post)
	if nil != err {
		return err
	}

	// json
	json := lygo_paths.Concat(target, fmt.Sprintf("%v.json", name))
	data := &ThemiflyPostMeta{
		Lang:        "it",
		Title:       "Post Title",
		Description: "Post Excerpt Here.... Write some text to summarize the content.",
		Image:       fmt.Sprintf("./%v.png", name),
		Author:      "Your Name Here",
		AuthorImage: "",
		AuthorLink:  "",
		Date:        lygo_date.FormatDate(time.Now(), lygo_date.DatePatterns[0]),
		Categories:  make([]string, 0),
		Tags:        make([]string, 0),
	}
	_, err = lygo_io.WriteTextToFile(lygo_json.Stringify(data), json)

	return err
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ThemiFly) init(filename string) (err error) {
	if len(filename) == 0 {
		instance.dirWork = lygo_paths.GetWorkspacePath()
		filename = lygo_paths.Concat(instance.dirWork, "settings.json")
	} else {
		filename = lygo_paths.Absolute(filename)
		if b, _ := lygo_paths.IsFile(filename); b {
			instance.dirWork = lygo_paths.Dir(filename)
		} else {
			instance.dirWork = filename
			filename = lygo_paths.Concat(instance.dirWork, "settings.json")
		}
	}

	instance.dirWork = lygo_paths.Absolute(instance.dirWork)
	instance.dirTemplate = lygo_paths.ConcatDir(instance.dirWork, "template")
	instance.dirUploads = lygo_paths.ConcatDir(instance.dirWork, "uploads")
	instance.dirBlocks = lygo_paths.ConcatDir(instance.dirWork, "blocks")
	instance.dirBlog = lygo_paths.ConcatDir(instance.dirWork, "blog")

	err = lygo_paths.Mkdir(lygo_paths.ConcatDir(instance.dirTemplate))
	if nil != err {
		return
	}
	err = lygo_paths.Mkdir(lygo_paths.ConcatDir(instance.dirUploads))
	if nil != err {
		return
	}
	err = lygo_paths.Mkdir(lygo_paths.ConcatDir(instance.dirBlocks))
	if nil != err {
		return
	}
	err = lygo_paths.Mkdir(lygo_paths.ConcatDir(instance.dirBlog))
	if nil != err {
		return
	}

	// load settings
	instance.settings, err = LoadSettings(filename)
	if nil != err {
		return
	}

	// ticker
	instance.ticker = lygo_events.NewEventTicker(20*time.Second, instance.onTick)

	// indexer: template indexer
	instance.indexer = NewIndexer(instance.dirTemplate, instance.dirBlocks)

	instance.vfs, err = vfs(instance.dirWork, instance.settings.TargetDir)

	return
}

func (instance *ThemiFly) onTick(_ *lygo_events.EventTicker) {
	if nil != instance {
		instance.checkUploads()
	}
}

func (instance *ThemiFly) checkUploads() {
	m := instance.mapFiles(instance.dirUploads)
	if nil != m {
		for _, v := range m {
			instance.publish(v)
		}
	}
}

func (instance *ThemiFly) mapFiles(dir string) map[string]map[string]interface{} {
	files, _ := lygo_paths.ReadFileOnly(dir)
	if len(files) > 0 {
		return group(files)
	}
	return nil
}

func (instance *ThemiFly) publish(files map[string]interface{}) {
	if nil == files || len(files) == 0 {
		return
	}
	if html, b := files["html"]; b {
		fileHTML := lygo_paths.Concat(instance.dirUploads,
			fmt.Sprintf("%v", html))
		content, err := lygo_io.ReadTextFromFile(fileHTML)
		if nil == err {
			var fileJSON, fileIMAGE string
			var meta *ThemiflyPostMeta
			if json, b := files["json"]; b {
				fileJSON = lygo_paths.Concat(instance.dirUploads, fmt.Sprintf("%v", json))
				meta = loadMeta(fileJSON)
				// verify image is base64
				if !strings.HasPrefix(meta.Image, "data:image") {
					fileIMAGE = lygo_paths.Concat(instance.dirUploads, meta.Image)
					if b, _ := lygo_paths.Exists(fileIMAGE); b {
						if bytes, e := lygo_io.ReadBytesFromFile(fileIMAGE); nil == e {
							meta.Image = "data:image/png;base64," + lygo_crypto.EncodeBase64(bytes)
						}
					}
				}
			}
			if nil == meta {
				meta = getMeta(&ThemiflyPostMeta{})
			}
			// publish the post
			_ = instance.Publish(meta, content)
			if instance.settings.Blog.KeepUploadedPost {
				dir := lygo_paths.DatePath(lygo_paths.Concat(instance.dirUploads, "backup"), "", 6, true)
				dir = lygo_paths.ConcatDir(dir)
				if len(fileHTML) > 0 {
					_ = lygo_io.MoveFile(fileHTML, dir)
				}
				if len(fileJSON) > 0 {
					_ = lygo_io.MoveFile(fileJSON, dir)
				}
				if len(fileIMAGE) > 0 {
					_ = lygo_io.MoveFile(fileIMAGE, dir)
				}
			} else {
				// remove
				if len(fileHTML) > 0 {
					_ = lygo_io.Remove(fileHTML)
				}
				if len(fileJSON) > 0 {
					_ = lygo_io.Remove(fileJSON)
				}
				if len(fileIMAGE) > 0 {
					_ = lygo_io.Remove(fileIMAGE)
				}
			}
		}
	}
}
