module bitbucket.org/lygo/lygo_ext_themifly

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.110
	bitbucket.org/lygo/lygo_events v0.1.5 // indirect
	bitbucket.org/lygo/lygo_ext_vfs v0.1.5 // indirect
	github.com/Djarvur/go-err113 v0.1.0 // indirect
	github.com/ashanbrown/makezero v0.0.0-20210308000810-4155955488a0 // indirect
	github.com/cbroglie/mustache v1.2.0 // indirect
	github.com/esimonov/ifshort v1.0.2 // indirect
	github.com/go-critic/go-critic v0.5.5 // indirect
	github.com/golangci/golangci-lint v1.38.0 // indirect
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/gostaticanalysis/analysisutil v0.7.1 // indirect
	github.com/jingyugao/rowserrcheck v0.0.0-20210315055705-d907ca737bb1 // indirect
	github.com/julz/importas v0.0.0-20210228071311-d0bf5cb4e1db // indirect
	github.com/magiconair/properties v1.8.5 // indirect
	github.com/mattn/go-runewidth v0.0.10 // indirect
	github.com/mgechev/revive v1.0.5 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/pkg/sftp v1.13.0 // indirect
	github.com/quasilyte/go-ruleguard v0.3.2 // indirect
	github.com/quasilyte/regex/syntax v0.0.0-20200805063351-8f842688393c // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/sanposhiho/wastedassign v0.2.0 // indirect
	github.com/securego/gosec/v2 v2.7.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/afero v1.6.0 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/tomarrell/wrapcheck v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2 // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/net v0.0.0-20210323141857-08027d57d8cf // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	honnef.co/go/tools v0.1.3 // indirect
	mvdan.cc/gofumpt v0.1.1 // indirect

)
