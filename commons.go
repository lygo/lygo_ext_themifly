package lygo_ext_themifly

import (
	"bitbucket.org/lygo/lygo_commons/lygo_array"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_crypto"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_vfs"
	"bitbucket.org/lygo/lygo_ext_vfs/commons"
	"errors"
	"github.com/cbroglie/mustache"
	"golang.org/x/net/html"
	"strings"
)

const (
	Name    = "ThemiFly"
	Version = "0.1.1"

	SelectorThemiflyWidgetBlogPostHere          = "themifly-widget-blog-posts-here"
	SelectorThemiflyWidgetBlogAuthorHere        = "themifly-widget-blog-author-here"
	SelectorThemiflyWidgetBlogTagsAllHere       = "themifly-widget-blog-tags-all-here"
	SelectorThemiflyWidgetBlogCategoriesHere    = "themifly-widget-blog-categories-here"
	SelectorThemiflyWidgetBlogCategoriesAllHere = "themifly-widget-blog-categories-all-here"
	SelectorThemiflyWidgetBlogPrevNextHere      = "themifly-widget-blog-prev-next-here"
	SelectorThemiflyWidgetBlogPostContentHere   = "themifly-widget-blog-post-content-here"

	BlockTypeHTML     = "html"   // simple text to replace
	BlockTypeWidget   = "widget" // dynamic widget
	BlockTypeMustache = "mustache"

	FlagNoOverwrite = "no_overwrite"
)

var (
	ErrorMissingWidgetSelector   = errors.New("missing_widget_selector")
	ErrorMissingBlogTemplate     = errors.New("missing_blog_template")
	ErrorMissingBlogPostTemplate = errors.New("missing_blog_post_template")
	ErrorMissingBlock            = errors.New("missing_block")
	ErrorFileNotFound            = errors.New("file_not_found")
	ErrorUnableToAppendNode      = errors.New("unable_to_append_node")
)

func isHTML(text string) bool {
	return strings.Index(text, "<") > -1 && strings.Index(text, "/") > 0
}

func isBase64Image(text string) bool {
	return strings.HasPrefix(text, "data:image")
}

func relativizePath(path string) string {
	if !strings.HasPrefix(path, "./") {
		return "./" + path
	}
	return path
}

func vfs(absRoot string, settings *ThemiflySettingsTarget) (commons.IVfs, error) {
	vfs, err := lygo_ext_vfs.NewVfs(settings)
	if nil != err {
		return nil, err
	}
	if len(settings.Home) > 0 {
		_, err = vfs.Cd(settings.Home)
		if nil != err {
			return nil, err
		}
	}

	scheme, host := settings.SplitLocation()
	if scheme == commons.SchemaOS && !lygo_paths.IsAbs(host) {
		_, err = vfs.Cd(lygo_paths.Concat(absRoot, host))
	}

	return vfs, err
}

func vfsSave(vfs commons.IVfs, doc *html.Node, pageName string) (err error) {
	parser := NewHtmlParser()
	pageHTML := parser.RenderNode(doc)
	_, err = vfs.Write([]byte(pageHTML), pageName)
	return
}

func vfsSaveImage(vfs commons.IVfs, text string) (uri string, err error) {
	if isBase64Image(text) {
		base := strings.Replace(text, "data:image/jpeg;base64,", "", 1)
		base = strings.Replace(base, "data:image/png;base64,", "", 1)
		// decode
		bytes, decError := lygo_crypto.DecodeBase64(strings.TrimSpace(base))
		if nil != decError {
			return "", decError
		}

		name := lygo_crypto.MD5(base)
		uri = "images/" + name + ".png"
		_, err = vfs.Write(bytes, "./"+uri)
	}
	return
}

func vfsRead(vfs commons.IVfs, pageName string) (string, error) {
	if b, _ := vfs.Exists(pageName); b {
		data, err := vfs.Read(pageName)
		if nil != err {
			return "", err
		}
		return string(data), nil
	}
	return "", lygo_errors.Prefix(ErrorFileNotFound, pageName)
}

func vfsReadOrCreate(vfs commons.IVfs, pageName string, defContent string) (string, error) {
	if b, _ := vfs.Exists(pageName); b {
		data, err := vfs.Read(pageName)
		if nil != err {
			return "", err
		}
		return string(data), nil
	} else {
		_, err := vfs.Write([]byte(defContent), pageName)
		if nil != err {
			return "", err
		}
		return defContent, err
	}
}

func merge(params ...interface{}) map[string]interface{} {
	response := make(map[string]interface{})
	for _, param := range params {
		m := lygo_conv.ToMap(param)
		if nil != m {
			lygo_reflect.ExtendMap(response, m, false)
		}
	}
	return response
}

func renderDocument(parser *HtmlParser, pageRawHtml string, blocks []*BlockItem, data map[string]interface{}) (*html.Node, error) {
	// replace blocks and render HTML
	renderedHTML, _, err := renderBlocks(pageRawHtml, blocks, data)
	if nil != err {
		return nil, err
	}

	// create document
	doc, err := parser.WrapText(renderedHTML)
	if nil != err {
		return nil, err
	}
	if nil == doc {
		return nil, ErrorMissingBlogTemplate
	}

	return doc, nil
}

func renderBlocks(pageRawHtml string, blocks []*BlockItem, data map[string]interface{}) (renderedHTML string, replaced map[string]interface{}, err error) {
	// get data from mustache blocks
	blocksData := getBlocksMustacheData(blocks)

	// pre-rendering to avoid broken HTML
	renderedHTML, err = render(pageRawHtml, data, blocksData)

	// replace existing blocks (HTML, WIDGETS)
	renderedHTML, replaced = replaceBlocks(renderedHTML, blocks)

	// render HTML
	renderedHTML, err = render(renderedHTML, data, blocksData)
	if nil != err {
		return "", nil, err
	}
	return
}

func render(template string, model ...interface{}) (string, error) {
	return mustache.Render(template, model...)
}

func replaceBlocks(text string, blocks []*BlockItem) (string, map[string]interface{}) {
	replaced := map[string]interface{}{}
	for _, block := range blocks {
		if len(block.Match) > 0 && len(block.Replace) > 0 {
			switch block.Type {
			case BlockTypeHTML:
				// replace
				t, count := replaceText(text, block.Match, block.Replace)
				if count > 0 {
					text = t
				}
				replaced[block.Name] = count
			case BlockTypeWidget:
				// append
				t, b := replaceWidget(text, block.Match, block.Replace, block.Flags)
				if b && len(t) > 0 {
					text = t
					replaced[block.Name] = 1
				} else {
					replaced[block.Name] = 0
				}
			}
		}
	}
	return text, replaced
}

func replaceWidget(text, selector, childHTML string, flags []string) (string, bool) {
	parser := NewHtmlParser()
	doc, err := parser.WrapText(text)
	if nil != err || nil == doc {
		return "", false
	}
	found := false
	parser.ForEach(doc, func(node *html.Node) bool {
		if parser.Matches(node, selector) {
			// found
			found = true
			noOverwrite := lygo_array.IndexOf(FlagNoOverwrite, flags) > -1
			hasChildren := parser.CountChildrenNoText(node) > 0
			canClean := !noOverwrite && hasChildren
			canAppend := !noOverwrite || (noOverwrite && !hasChildren)
			if canClean {
				// remove children
				parser.RemoveChildren(node)
			}
			if canAppend {
				parser.AppendChildText(childHTML, node)
			}

		}
		return false // next node
	})
	if found {
		return parser.RenderNode(doc), true
	}

	return "", false
}

func replaceText(text, old, new string) (string, int) {
	count := 0
	if strings.Index(text, old) > -1 {
		// RAW TEXT REPLACEMENT
		for {
			if strings.Index(text, old) == -1 {
				break
			}
			count++
			text = strings.Replace(text, old, new, 1)
		}
		return text, 1
	}

	parser := NewHtmlParser()
	doc, err := parser.WrapText(text)
	if nil == err {
		nOld := parser.TryWrapTextNode(old)
		if nil != nOld {
			parser.ForEach(doc, func(node *html.Node) bool {
				clean := lygo_strings.Slugify(parser.RenderNode(node), "")
				match := lygo_strings.Slugify(parser.RenderNode(nOld), "")
				if clean == match {
					// found match
					nNew := parser.TryWrapTextNode(new)
					if nil != nNew {
						count++
						node.Parent.InsertBefore(nNew, node)
						node.Parent.RemoveChild(node)
					}
				}
				return false // continue
			})
			if count > 0 {
				text = parser.RenderNode(doc)
			}
		}
	}

	return text, count
}

func readMatchReplacePair(dirBlocks, dir string) (match, replace string, err error) {
	match, err = lygo_io.ReadTextFromFile(lygo_paths.Concat(dirBlocks, dir, "match.html"))
	if nil != err {
		match, err = lygo_io.ReadTextFromFile(lygo_paths.Concat(dirBlocks, dir, "match.txt"))
		if nil != err {
			match = "" // match is
		}
	}
	replace, err = lygo_io.ReadTextFromFile(lygo_paths.Concat(dirBlocks, dir, "replace.html"))
	if nil != err {
		return
	}
	return
}

func getBlockWidget(blocks []*BlockItem, uid string) (*BlockItem, error) {
	for _, block := range blocks {
		if block.WidgetUID == uid {
			return block, nil
		}
	}
	return nil, lygo_errors.Prefix(ErrorMissingBlock, uid)
}

func getBlocksMustacheData(blocks []*BlockItem) map[string]interface{} {
	response := make(map[string]interface{})
	for _, block := range blocks {
		if block.Type == BlockTypeMustache {
			response[block.Match] = block.Replace
		}
	}
	return response
}

func group(files []string) map[string]map[string]interface{} {
	response := make(map[string]map[string]interface{}, 0)
	for _, file := range files {
		base := lygo_paths.Dir(file)
		name := lygo_paths.FileName(file, false)
		ext := lygo_paths.ExtensionName(file)
		uid := lygo_crypto.MD5(base + name)
		if _, b := response[uid]; !b {
			response[uid] = make(map[string]interface{})
		}
		m := response[uid]
		m[ext] = file
	}
	return response
}

func readFile(file string) []byte {
	data, err := lygo_io.ReadBytesFromFile(file)
	if nil == err {
		return data
	}
	return make([]byte, 0)
}
