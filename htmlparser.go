package lygo_ext_themifly

import (
	"bitbucket.org/lygo/lygo_commons/lygo_array"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bytes"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
	"io"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//	Indexer
// ---------------------------------------------------------------------------------------------------------------------

type HtmlParser struct {
}

func NewHtmlParser() *HtmlParser {
	instance := new(HtmlParser)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	Indexer    p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *HtmlParser) WrapText(text string) (*html.Node, error) {
	return html.Parse(strings.NewReader(text))
}

func (instance *HtmlParser) WrapTextNodes(text string) ([]*html.Node, error) {
	nodes, err := html.ParseFragment(strings.NewReader(text), &html.Node{
		Type:     html.ElementNode,
		Data:     "body",
		DataAtom: atom.Body,
	})
	if nil != err {
		return nil, err
	}
	return nodes, err
}

func (instance *HtmlParser) WrapTextNode(text string) (*html.Node, error) {
	nodes, err := instance.WrapTextNodes(text)
	if nil == err && len(nodes) > 0 {
		return nodes[0], nil
	}
	return nil, err
}

func (instance *HtmlParser) TryWrapTextNode(text string) *html.Node {
	nodes, err := instance.WrapTextNodes(text)
	if nil == err && len(nodes) > 0 {
		return nodes[0]
	}
	return nil
}

func (instance *HtmlParser) WrapFile(filename string) (*html.Node, error) {
	text, err := lygo_io.ReadTextFromFile(filename)
	if nil != err {
		return nil, err
	}
	return html.Parse(strings.NewReader(text))
}

func (instance *HtmlParser) Lang(n *html.Node) string {
	lang := ""
	if nil != instance {
		instance.ForEach(n, func(node *html.Node) bool {
			lang = instance.GetAttr(node, "lang")
			return len(lang) > 0 // next node?
		})
	}
	return lang
}

func (instance *HtmlParser) Title(n *html.Node) string {
	title := ""
	if nil != instance {
		instance.ForEach(n, func(node *html.Node) bool {
			if strings.ToLower(node.Data) == "title" {
				title = instance.InnerHtml(node)
			}
			return len(title) > 0 // next node?
		})
	}
	return title
}

func (instance *HtmlParser) MetaTitle(n *html.Node) string {
	value := ""
	if nil != instance {
		value = instance.GetMetaContent(n, "title")
	}
	return value
}

func (instance *HtmlParser) MetaDescription(n *html.Node) string {
	value := ""
	if nil != instance {
		value = instance.GetMetaContent(n, "description")
	}
	return value
}

func (instance *HtmlParser) MetaAuthor(n *html.Node) string {
	value := ""
	if nil != instance {
		value = instance.GetMetaContent(n, "author")
	}
	return value
}

func (instance *HtmlParser) MetaKeywords(n *html.Node) []string {
	if nil != instance {
		return lygo_strings.SplitTrimSpace(instance.GetMetaContent(n, "keywords"), ",")
	}
	return []string{}
}

func (instance *HtmlParser) GetMetaContent(n *html.Node, name string) string {
	value := ""
	if nil != instance {
		instance.ForEach(n, func(node *html.Node) bool {
			if strings.ToLower(node.Data) == "meta" {
				attrName := instance.GetAttr(node, "name")
				if attrName == name {
					value = instance.GetAttr(node, "content")
				}
			}
			return len(value) > 0 // next node?
		})
	}
	return value
}

func (instance *HtmlParser) RenderNode(n *html.Node) string {
	if nil != n {
		var buf bytes.Buffer
		w := io.Writer(&buf)
		_ = html.Render(w, n)
		return buf.String()
	}
	return ""
}

func (instance *HtmlParser) ExtractText(node *html.Node) string {
	var buf bytes.Buffer
	var text string
	if node.Type==html.TextNode{
		 text = lygo_strings.Clear(instance.RenderNode(node))
	}
	if len(text) > 0 {
		buf.WriteString(text + "\n")
	}
	for sub := node.FirstChild; sub != nil; sub = sub.NextSibling {
		text = "" // reset text
		if sub.Data != "title"  {
			if sub.Type== html.TextNode{
				text = lygo_strings.Clear(instance.RenderNode(sub))
			} else if nil != sub.FirstChild && sub.FirstChild.Type == html.TextNode{
				text = lygo_strings.Clear(instance.RenderNode(sub.FirstChild))
			}
			if len(text) > 0 {
				buf.WriteString(text + "\n")
			}
		}
	}
	return buf.String()
}

func (instance *HtmlParser) IsTitle(node *html.Node) (bool, int) {
	tag := strings.ToLower(node.Data)
	if len(tag) == 2 && strings.Index(tag, "h") == 0 {
		level := tag[1]
		return true, lygo_conv.ToInt(string(level))
	}
	return false, -1
}

func (instance *HtmlParser) ForEach(node *html.Node, callback func(node *html.Node) bool) {
	if nil != node {
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			if child.Type == html.ElementNode || nil != child.FirstChild {
				exit := callback(child)
				if exit {
					break
				}
				instance.ForEach(child, callback)
			}
		}
	}
}

func (instance *HtmlParser) CountChildren(node *html.Node) int {
	count := 0
	if nil != node {
		for c := node.FirstChild; c != nil; c = c.NextSibling {
			count++
		}
	}
	return count
}

func (instance *HtmlParser) CountChildrenNoText(node *html.Node) int {
	count := 0
	if nil != node {
		for c := node.FirstChild; c != nil; c = c.NextSibling {
			if c.Type != html.TextNode && c.Type != html.CommentNode {
				count++
			}
		}
	}
	return count
}

func (instance *HtmlParser) Children(node *html.Node) []*html.Node {
	children := make([]*html.Node, 0)
	if nil != node {
		for c := node.FirstChild; c != nil; c = c.NextSibling {
			children = append(children, c)
		}
	}
	return children
}

func (instance *HtmlParser) RemoveChildren(node *html.Node) {
	if nil != node {
		children := instance.Children(node)
		for _, c := range children {
			node.RemoveChild(c)
		}
	}
}

func (instance *HtmlParser) AppendChildText(text string, parent *html.Node) bool {
	if len(text) > 0 && nil != parent {
		post := instance.TryWrapTextNode(text)
		if nil != post {
			// append
			parent.AppendChild(post)
			return true
		}
	}
	return false
}

func (instance *HtmlParser) AppendChild(child *html.Node, parent *html.Node) bool {
	if nil != child && nil != parent {
		// append
		parent.AppendChild(child)
		return true
	}
	return false
}

func (instance *HtmlParser) InsertChildText(text string, parent *html.Node) bool {
	if len(text) > 0 && nil != parent {
		child := instance.TryWrapTextNode(text)
		if nil != child {
			return instance.InsertChild(child, parent)
		}
	}
	return false
}

func (instance *HtmlParser) InsertChild(child *html.Node, parent *html.Node) bool {
	if nil != child && nil != parent {
		if nil!=parent.FirstChild{
			// append
			parent.InsertBefore(child, parent.FirstChild)
		} else {
			parent.AppendChild(child)
		}

		return true
	}
	return false
}

func (instance *HtmlParser) InsertBeforeText(parent *html.Node, oldChild *html.Node, newChild string) bool {
	if len(newChild) > 0 && nil != parent {
		n := instance.TryWrapTextNode(newChild)
		if nil != n {
			// append
			parent.InsertBefore(n, oldChild)
			return true
		}
	}
	return false
}

func (instance *HtmlParser) OuterHtml(n *html.Node) string {
	if nil != instance {
		return instance.RenderNode(n)
	}
	return ""
}

func (instance *HtmlParser) InnerHtml(n *html.Node) string {
	if nil != instance {
		return instance.RenderNode(n.FirstChild)
	}
	return ""
}

func (instance *HtmlParser) GelLinks(node *html.Node) []*html.Node {
	if nil != instance {
		return instance.QueryNodes(node, "a")
	}
	return []*html.Node{}
}

func (instance *HtmlParser) GetLinkURLs(node *html.Node) []string {
	response := make([]string, 0)
	if nil != instance {
		links := instance.QueryNodes(node, "a")
		for _, link := range links {
			href := instance.GetAttr(link, "href")
			if len(href) > 0 && strings.Index(href, "#") != 0 {
				response = lygo_array.AppendUnique(response, href).([]string)
			}
		}
	}
	return response
}

func (instance *HtmlParser) GeNodeAttributes(nodes []*html.Node) []map[string]string {
	response := make([]map[string]string, 0)
	if nil != instance {
		for _, node := range nodes {
			m := map[string]string{
				"tag": node.Data,
			}
			for _, attr := range node.Attr {
				m[attr.Key] = attr.Val
			}
			response = append(response, m)
		}
	}
	return response
}

func (instance *HtmlParser) GetAttr(node *html.Node, name string) string {
	if nil != node && len(node.Attr) > 0 {
		for _, attr := range node.Attr {
			if strings.ToLower(attr.Key) == strings.ToLower(name) {
				return attr.Val
			}
		}
	}
	return ""
}

func (instance *HtmlParser) QueryBody(root *html.Node) *html.Node {
	nodes := instance.QueryNodes(root, "body")
	if len(nodes) > 0 {
		return nodes[0]
	}
	return nil
}

func (instance *HtmlParser) QueryNodes(root *html.Node, selector string) []*html.Node {
	response := make([]*html.Node, 0)
	instance.ForEach(root, func(node *html.Node) bool {
		if instance.Matches(node, selector) {
			response = append(response, node)
		}
		return false // next node
	})
	return response
}

func (instance *HtmlParser) QueryNode(root *html.Node, selector string) *html.Node {
	response := instance.QueryNodes(root, selector)
	if len(response) > 0 {
		return response[0]
	}
	return nil
}

func (instance *HtmlParser) Matches(node *html.Node, selector string) bool {
	if strings.Index(selector, ".") == 0 {
		// class matching
		className := strings.Replace(selector, ".", "", 1)
		classes := strings.Split(instance.GetAttr(node, "class"), " ")
		return lygo_array.IndexOf(className, classes) > -1
	} else {
		// tag name matching
		return node.Data == selector
	}
}
