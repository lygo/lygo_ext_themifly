package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_themifly"
	"fmt"
	"testing"
)


func TestAll(t *testing.T) {
	// create engine
	themifly, err := lygo_ext_themifly.NewThemiFly("./workspace")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// open engine
	themifly.Open()

	response, err := themifly.Build()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(lygo_json.Stringify(response))

	// themifly.Wait()
}

func TestPublish(t *testing.T) {
	// create engine
	themifly, err := lygo_ext_themifly.NewThemiFly("./workspace")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = themifly.ResetBlogData()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// open engine
	themifly.Open()

	err = themifly.Publish(&lygo_ext_themifly.ThemiflyPostMeta{
		Title:       "HELLO",
		Description: "This is a TEST POST",
		Tags:        []string{"tag3", "tag4"},
		Categories:  []string{"cat1", "cat2"},
		Author:      "Angelo",
		Date:        "20210325",
	},
		"<strong>Post</strong>",
	)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	//fmt.Println(lygo_json.Stringify(response))

	// themifly.Wait()
}

func TestAutoPublish(t *testing.T) {
	// create engine
	themifly, err := lygo_ext_themifly.NewThemiFly("./workspace")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	err = generateStub(themifly)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	_ = themifly.ResetBlogData()

	// open engine
	themifly.Open()

	themifly.Wait()
}

func TestGeneratePostStub(t *testing.T) {
	// create engine
	themifly, err := lygo_ext_themifly.NewThemiFly("./workspace")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	err = generateStub(themifly)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func generateStub(themifly *lygo_ext_themifly.ThemiFly) error {
	// create engine
	return  themifly.GenerateBlogPostStub("post", "./uploads")
}
