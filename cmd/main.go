package main

import (
	"bitbucket.org/lygo/lygo_commons/lygo_fmt"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_themifly"
	"flag"
	"fmt"
	"os"
)

// ---------------------------------------------------------------------------------------------------------------------
//	m a i n
// ---------------------------------------------------------------------------------------------------------------------

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := lygo_strings.Format("[panic] APPLICATION %s ERROR: %s", lygo_ext_themifly.Name, r)
			fmt.Println(message)
		}
	}()

	//-- command flags --//
	// build
	cmdBuild := flag.NewFlagSet("build", flag.ExitOnError)
	cmdBuildSettings := cmdBuild.String("settings", lygo_paths.Absolute("./_workspace/settings.json"), "Set file to load with required settings")
	// publish
	cmdPublish := flag.NewFlagSet("publish", flag.ExitOnError)
	cmdPublishSettings := cmdPublish.String("settings", lygo_paths.Absolute("./_workspace/settings.json"), "Set file to load with required settings")
	cmdPublishDir := cmdPublish.String("dir", lygo_paths.Absolute("./uploads"), "Set directory with post to publish")
	// stub
	cmdStub := flag.NewFlagSet("stub", flag.ExitOnError)
	cmdStubSettings := cmdStub.String("settings", lygo_paths.Absolute("./_workspace/settings.json"), "Set file to load with required settings")
	cmdStubDir := cmdStub.String("dir", "./uploads", "Set target directory")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "build":
			// BUILD
			_ = cmdBuild.Parse(os.Args[2:])
			dirWork := lygo_paths.Dir(*cmdBuildSettings)
			// set paths
			lygo_paths.GetWorkspace("*").SetPath(dirWork)
			// run command
			build(*cmdBuildSettings)
		case "publish":
			// BUILD
			_ = cmdPublish.Parse(os.Args[2:])
			dirWork := lygo_paths.Dir(*cmdPublishSettings)
			// set paths
			lygo_paths.GetWorkspace("*").SetPath(dirWork)
			// run command
			publish(*cmdPublishSettings, *cmdPublishDir)
		case "stub":
			// STUB
			_ = cmdStub.Parse(os.Args[2:])
			dirWork := lygo_paths.Dir(*cmdStubSettings)
			// set paths
			lygo_paths.GetWorkspace("*").SetPath(dirWork)
			dir := *cmdStubDir
			if len(dir) == 0 {
				dir = "./uploads"
			}
			// run command
			stub(*cmdStubSettings, dir)
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func build(file string) {
	themifly, err := lygo_ext_themifly.NewThemiFly(file)
	if nil != err {
		panic(err)
	}

	// open engine
	themifly.Open()

	response, err := themifly.Build()
	if nil != err {
		panic(err)
	}

	fmt.Print(lygo_fmt.FormatMap(response))
}

func publish(file, dir string) {
	themifly, err := lygo_ext_themifly.NewThemiFly(file)
	if nil != err {
		panic(err)
	}

	// open engine
	themifly.Open()

	err = themifly.PublishDir(dir)
	if nil != err {
		panic(err)
	}

	fmt.Print("DONE. Post published.")
}

func stub(file, target string) {
	themifly, err := lygo_ext_themifly.NewThemiFly(file)
	if nil != err {
		panic(err)
	}

	err = themifly.GenerateBlogPostStub("new_post", target)
	if nil != err {
		panic(err)
	}

	fmt.Print(fmt.Sprintf("Stub generated into '%v'", target))
}
