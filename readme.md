# LyGo ThemiFly #
## CONSOLE EDITION ##

![](icon.png)

ThemiFly is simple (cross platform) console application that allow you to manage a static website
in pure HTML with the power of:

 - Mustache like template engine
 - Dynamic blog management with posts
 - Virtual File System (SFTP, OS) deploy

ThemiFly is cross platform and support:

 - Windows
 - Mac OSX
 - Linux
 - Raspberry 3/x

## How does it work ##

ThemiFly need a "workspace" (a working directory) containing required files.

![](./_docs/workspace.png)


### Settings ###

First file you have to check is **settings.json**.
```json
{
  "target-dir": {
    "location": "file://../deploy-dir",
    "auth": {
      "user": "",
      "password": ""
    }
  },
  "blog": {
    "post-per-page": 5,
    "keep-uploaded-post": false
  },
  "site-data": {
    "site-author": "Gian Angelo Geminiani",
    "site-title": "Themifly",
    "site-description": "Really simple site manager",
    "og-description": "ThemiFly web site management tool",
    "og-image": "https://www.gianangelogeminiani.me/preview.png",
    "og-site_name": "themifly",
    "og-title": "Themifly",
    "og-type": "website",
    "og-url": "https://www.gianangelogeminiani.me",
    "twitter-card": "summary",
    "twitter-site": "https://www.gianangelogeminiani.me",
    "twitter-creator": "@angelogeminiani",
    "twitter-title": "Themifly",
    "twitter-description": "ThemiFly web site management tool",
    "twitter-image": "https://www.gianangelogeminiani.me/preview.png"
  }
}
```

Settings fields are quite simple to understand and below is a quick explanation:

 - `target-dir`: This is a VFS (Virtual File System) configuration block. 
   VFS works on both OS file system (if you need deploy locally),
   or on SFTP (if you prefer deploy remotely).
   - `location`: Here you must declare URL of target dir for VFS. Supported protocols are `file://` and `sftp://`.
    The location landing point is the home of current user (i.e. if you are working as root user on remote linux environment the landing path will be "/root").
   - `auth`: (Only for remote VFS) This is the authentication object containing username and password. This is not used in local configurations using "file://" protocol. 
 - `blog`: Blog settings
   - `post-per-page`: Number of max post contained in a page. Default is "5"
   - `keep-uploaded-post`: If "true", published post files are saved in a backup folder. Default is "false"
 - `site-data`: This is a model (data to replace in HTML template files) used to render templates and produce pages.
 

## Blocks and Templates ##

ThemiFly rules are described into files.

Those files are organized into 2 main groups:

 - Blocks: match and replace rules
 - Templates: original files that produce target html pages for your site.

Both blocks and templates can make use (mustache like template syntax) of fields declared in `site-data` section of settings.json configuration file.

### Blocks ###

Once you have configured your settings.json, is time to set blocks and templates.

**Blocks** are peace of text that look for a match into a template and are replaced with a "replace" value.
All blocks are declared as *folders*. In each block folder are contained 2 files:

 - match file
 - replace file

Match files (`match.html` or `match.txt`) are HTML or TXT files that ThemiFly
uses as fields to search into the template.
Below are HTML and TXT samples
```HTML
<!-- HTML sample match file -->
<div class="inner">
    <ul>
        <li><a href="#">Darkness</a></li>
        <li><a href="#">Goddes</a></li>
        <li><a href="#">Employee</a></li>
        <li><a href="#">Berry</a></li>
        <li><a href="#">Roosters</a></li>
        <li><a href="#">Primero</a></li>
    </ul>
</div>
```
```txt
#simple txt match file
themifly-widget-blog-author-here:no_overwrite
```
HTML files are used for one to one match. HTML replace algorithm look for node correspondence and replace the content with new replace HTML.

TXT match files are special objects we call "WIDGETS". A text tag is always a WIDGET declaration.
Most WIDGETS tags are simple names like `themifly-widget-blog-author-here`. Usually you do not need anything else.
But, depending on what is desired behaviour during replace, you can add an action verb separated from a column.

Below is a list of supported action verbs:
 - `no_overwrite`: do not overwrite existing content if already exists

Most used WIDGETS:

 - `themifly-widget-blog-author-here`: Used for blog author info
 - `themifly-widget-blog-categories-here`: Used for post categories 
 - `themifly-widget-blog-categories-all-here`: Used for blog categories
 - `themifly-widget-blog-tags-all-here`: Used for blog tags
 - `themifly-widget-blog-prev-next-here`: Used for blog Prev. Next. navigation buttons
 - `themifly-widget-social-share-here`: Used for social media sharing buttons

NOTE: **WIDGETS are not hard coded, and you can create as many as you want!**

This is a very important concept: you can define and use as many widgets you want.

HTML and TXT (widgets tag) are used to locate a node into templates.
Once node is located, ThemiFly replace existing matched text (or html nodes) with new text (or html nodes).

Let's have a look at REPLACER (replace.html) for `themifly-widget-blog-author-here` WIDGET: 
```html
<div class="post-author">
    {{#author-image}}
    <img src="{{author-image}}" alt="Image">
    {{/author-image}}
    {{^author-image}}
    <img src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==" alt="Image"/>
    {{/author-image}}
    <span>by <a href="{{author-link}}">{{author}}</a></span>
</div>
```

**EXPLANATION**: 

ThemiFly loop on all templates (see at 'templates' paragraph) and look for detect a MATCH object (HTML or WIDGET).
Once the MATCH object is detected, ThemiFly try to replace the matched data with new HTML nodes (those declared into replace.html files).
The replace.html file may contain [mustache expressions](http://mustache.github.io/mustache.5.html) that are resolved from ThemiFly.

WIDGETS must be declared as custom html tags in templates. Ex: `<themifly-widget-blog-author-here></themifly-widget-blog-author-here>`.

WARNING! : Do not use short notation (`<themifly-widget-blog-author-here/>`). 
Instead, use full notation for a better matching from ThemiFly engine.

### Templates ###
TODO: write doc

Sample WIDGET declaration in template:
```html
<div class="post">
    <figure class="post-image">
        <img src="{{image}}" alt="Image">
    </figure>
    <div class="post-content">
        <small class="post-date">
            {{date}}
        </small>
        <h3 class="post-title"><a href="#">{{title}}</a></h3>

        <themifly-widget-blog-author-here>
        </themifly-widget-blog-author-here>
        <!-- end post-author -->

        <themifly-widget-blog-categories-here>
        </themifly-widget-blog-categories-here>
        <!-- end post categories -->

        <p>{{description}}...</p>
        <a href="{{blog-page-single}}" class="post-link">READ MORE</a>
    </div>
    <!-- end post-content -->
</div>
```

## MUSTACHE TEMPLATE ENGINE ##
[Mustache Manuals are here!](http://mustache.github.io/mustache.5.html)


## Use as a Module in your own code ##

ThemiFly is also a Go module and ca be imported with a simple `go get` request.

Dependencies:
```
go get -u bitbucket.org/lygo/lygo_commons 
go get -u bitbucket.org/lygo/lygo_events
go get -u bitbucket.org/lygo/lygo_ext_vfs 
```

To use just call:

```
go get -u bitbucket.org/lygo/lygo_ext_themifly@v0.1.0
```

### Versioning ###

Sources are versioned using git tags:

```
git tag v0.1.0
git push origin v0.1.0
```

