package lygo_ext_themifly

import (
	"bitbucket.org/lygo/lygo_commons/lygo_array"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_date"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_vfs/commons"
	"fmt"
	"golang.org/x/net/html"
	"math"
	"strings"
	"time"
)

/**
Blogger handle dynamic log pages generation.
*/

const (
	// page name template
	pageNameTemplate   = "blog-%v.html"
	singleNameTemplate = "blog-single-%v.html"
)

// ---------------------------------------------------------------------------------------------------------------------
//	ThemiflyData
// ---------------------------------------------------------------------------------------------------------------------

type ThemiflyBloggerData struct {
	PostCount     int            `json:"post-count"`
	PageCount     int            `json:"page-count"`
	PostPerPage   int            `json:"post-per-page"`
	TagsAll       map[string]int `json:"tags-all"`
	CategoriesAll map[string]int `json:"categories-all"`
}

type ThemiflyPostMeta struct {
	Title       string                 `json:"title"`
	Description string                 `json:"description"`
	Image       string                 `json:"image"`
	Tags        []string               `json:"tags"`
	Categories  []string               `json:"categories"`
	Author      string                 `json:"author"`
	AuthorLink  string                 `json:"author-link"`
	AuthorImage string                 `json:"author-image"`
	Date        string                 `json:"date"` // yyyyMMdd HH:mm
	Lang        string                 `json:"lang"`
	MicroData   *ThemiflyPostMicroMeta `json:"post-micro"`
	// late initialized
	Keywords       string      `json:"keywords"`
	BlogPage       string      `json:"blog-page"`                // late initialized
	BlogPageNext   string      `json:"blog-page-next,omitempty"` // late initialized
	BlogPagePrev   string      `json:"blog-page-prev,omitempty"` // late initialized
	BlogPageSingle string      `json:"blog-page-single"`         // late initialized
	TagsAll        interface{} `json:"tags-all"`                 // late initialized
	CategoriesAll  interface{} `json:"categories-all"`           // late initialized
}

type ThemiflyPostMicroMeta struct {
	OgTitle            string `json:"og-title"`
	OgDescription      string `json:"og-description"`
	OgImage            string `json:"og-image"`
	OgType             string `json:"og-type"`
	OgUrl              string `json:"og-url"`
	OgSiteName         string `json:"og-site_name"`
	TwitterTitle       string `json:"twitter-title"`
	TwitterDescription string `json:"twitter-description"`
	TwitterCard        string `json:"twitter-card"`
	TwitterCreator     string `json:"twitter-creator"`
	TwitterSite        string `json:"twitter-site"`
	TwitterImage       string `json:"twitter-image"`
}

// ---------------------------------------------------------------------------------------------------------------------
//	ThemiFlyBlogger
// ---------------------------------------------------------------------------------------------------------------------

type ThemiFlyBlogger struct {
	enabled       bool
	dirWork       string
	dirWorkBlog   string
	dataFile      string
	blockItems    []*BlockItem
	settings      *ThemiflyBlogSettings
	vfs           commons.IVfs
	bloggerData   *ThemiflyBloggerData
	siteData      map[string]interface{}
	tplBlog       string
	tplBlogPost   string
	tplBlogSingle string
}

func NewThemiFlyBlogger(dirWork, dirBlog string, blockItems []*BlockItem, settings *ThemiflySettings, vfs commons.IVfs) (*ThemiFlyBlogger, error) {
	instance := new(ThemiFlyBlogger)
	instance.dirWork = dirWork
	instance.dirWorkBlog = dirBlog
	instance.blockItems = blockItems
	instance.settings = settings.Blog
	instance.siteData = settings.SiteData
	instance.vfs = vfs

	instance.dataFile = lygo_paths.Concat(instance.dirWorkBlog, "index.json")

	err := instance.init()

	return instance, err
}

// ---------------------------------------------------------------------------------------------------------------------
//	ThemiFlyBlogger    p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ThemiFlyBlogger) ResetData() error {
	instance.bloggerData = new(ThemiflyBloggerData)
	return instance.saveData()
}

func (instance *ThemiFlyBlogger) Publish(i interface{}, content string) (err error) {
	if len(content) > 0 {
		meta := getMeta(i)
		if nil != meta {
			// add excerpt if missing
			if len(meta.Description) == 0 {
				parser := NewHtmlParser()
				meta.Description = lygo_strings.FillRight(parser.ExtractText(parser.TryWrapTextNode(content)), 255, ' ')
				meta.Description = strings.ReplaceAll(meta.Description, "\n", "<br>")
			}
			err = instance.post(meta, content)
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	ThemiFlyBlogger    p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *ThemiFlyBlogger) init() (err error) {
	// data
	if b, _ := lygo_paths.Exists(instance.dataFile); b {
		// load or create data
		text, _ := lygo_io.ReadTextFromFile(instance.dataFile)
		err = lygo_json.Read(text, &instance.bloggerData)
	} else {
		instance.bloggerData = new(ThemiflyBloggerData)
	}
	if nil != err {
		return
	}

	// update data
	instance.bloggerData.PostPerPage = instance.settings.PostPerPage
	err = instance.saveData()
	if nil != err {
		return
	}

	// load templates from file
	fileTplBlog := lygo_paths.Concat(instance.dirWorkBlog, "blog-page.html")
	instance.tplBlog, _ = lygo_io.ReadTextFromFile(fileTplBlog)
	fileTplBlogPost := lygo_paths.Concat(instance.dirWorkBlog, "blog-post.html")
	instance.tplBlogPost, _ = lygo_io.ReadTextFromFile(fileTplBlogPost)
	fileTplBlogSingle := lygo_paths.Concat(instance.dirWorkBlog, "blog-single.html")
	instance.tplBlogSingle, _ = lygo_io.ReadTextFromFile(fileTplBlogSingle)

	return
}

func (instance *ThemiFlyBlogger) saveData() (err error) {
	_, err = lygo_io.WriteTextToFile(lygo_json.Stringify(instance.bloggerData), instance.dataFile)
	return
}

func (instance *ThemiFlyBlogger) post(meta *ThemiflyPostMeta, content string) (err error) {
	prevPageCount := instance.bloggerData.PageCount
	if prevPageCount == 0 {
		prevPageCount = 1
	}
	instance.bloggerData.PageCount = blogPageId(instance.bloggerData.PostCount, instance.bloggerData.PostPerPage)
	pageName := fmt.Sprintf(pageNameTemplate, instance.bloggerData.PageCount)


	// update data withs meta info
	updateData(instance.bloggerData, meta)

	meta.BlogPage = pageName                                                                // the blog page
	meta.BlogPageSingle = fmt.Sprintf(singleNameTemplate, instance.bloggerData.PostCount+1) // single post page for full read
	if instance.bloggerData.PageCount > 1 {
		meta.BlogPagePrev = fmt.Sprintf(pageNameTemplate, prevPageCount)
	}

	// update microdata (include images)
	updateMicrodata(meta, instance.vfs)

	// merge
	data := merge(meta, instance.siteData)

	// get rendered content
	pageContent, e := vfsReadOrCreate(instance.vfs, relativizePath(pageName), instance.tplBlog)
	if nil != e {
		err = e
		return
	}
	pageContent, _, e = renderBlocks(pageContent, instance.blockItems, data)
	if nil != e {
		err = e
		return
	}

	// add post to page
	docBlog, err := instance.addPostToPage(pageContent, data)
	if nil != err {
		return
	}
	// ready to save new HTML
	err = vfsSave(instance.vfs, docBlog, relativizePath(meta.BlogPage))

	// create single post page
	docBlogSingle, err := instance.createBlogPageSingle(meta, instance.tplBlogSingle, content)
	if nil != err {
		return
	}
	// ready to save new HTML
	err = vfsSave(instance.vfs, docBlogSingle, relativizePath(meta.BlogPageSingle))

	// add link to previous page
	if prevPageCount != instance.bloggerData.PageCount {
		meta.BlogPageNext = pageName
		prevPageName := fmt.Sprintf(pageNameTemplate, prevPageCount)
		prevHTML, readErr := vfsRead(instance.vfs, relativizePath(prevPageName))
		if nil == readErr {
			prevDoc, updErr := instance.updateNextPrev(meta, prevHTML)
			if nil == updErr {
				err = vfsSave(instance.vfs, prevDoc, relativizePath(prevPageName))
			}
		}
	}

	// update data
	instance.bloggerData.PostCount++
	err = instance.saveData()

	return
}

func (instance *ThemiFlyBlogger) addPostToPage(pageRawHtml string, data map[string]interface{}) (*html.Node, error) {
	// replace blocks and render HTML into a document
	parser := NewHtmlParser()
	doc, err := renderDocument(parser, pageRawHtml, instance.blockItems, data)
	if nil != err {
		return nil, err
	}

	if parent := parser.QueryNode(doc, SelectorThemiflyWidgetBlogPostHere); nil != parent {
		// render the post
		post, renderError := renderDocument(parser, instance.tplBlogPost, instance.blockItems, data)
		//postHTML, renderError := render(instance.tplBlogPost, meta)
		if nil != renderError {
			return nil, renderError
		}

		if parser.InsertChild(post, parent) {
			return doc, nil
		} else {
			return nil, ErrorUnableToAppendNode
		}
	} else {
		return nil, ErrorMissingWidgetSelector
	}
}

func (instance *ThemiFlyBlogger) createBlogPageSingle(meta *ThemiflyPostMeta, pageRawHtml string, content string) (*html.Node, error) {
	data := merge(meta, instance.siteData)

	// replace blocks and render HTML into a document
	parser := NewHtmlParser()
	doc, err := renderDocument(parser, pageRawHtml, instance.blockItems, data)
	if nil != err {
		return nil, err
	}

	if parent := parser.QueryNode(doc, SelectorThemiflyWidgetBlogPostContentHere); nil != parent {
		// render the content
		blockHTML, renderError := render(content, data)
		if nil != renderError {
			return nil, renderError
		}
		if parser.AppendChildText(blockHTML, parent) {
			return doc, nil
		} else {
			return nil, ErrorUnableToAppendNode
		}
	} else {
		return nil, lygo_errors.Prefix(ErrorMissingWidgetSelector, SelectorThemiflyWidgetBlogPostContentHere)
	}
}

func (instance *ThemiFlyBlogger) updateNextPrev(meta *ThemiflyPostMeta, pageRawHtml string) (*html.Node, error) {
	data := merge(meta, instance.siteData)

	// replace blocks and render HTML into a document
	parser := NewHtmlParser()
	doc, err := renderDocument(parser, pageRawHtml, instance.blockItems, data)
	if nil != err {
		return nil, err
	}

	// get block
	block, err := getBlockWidget(instance.blockItems, SelectorThemiflyWidgetBlogPrevNextHere)
	if nil != err {
		return nil, err
	}

	if parent := parser.QueryNode(doc, SelectorThemiflyWidgetBlogPrevNextHere); nil != parent {
		// remove previous children
		parser.RemoveChildren(parent)
		// render the post
		blockHTML, renderError := render(block.Replace, data)
		if nil != renderError {
			return nil, renderError
		}
		if parser.AppendChildText(blockHTML, parent) {
			return doc, nil
		} else {
			return nil, ErrorUnableToAppendNode
		}
	} else {
		return nil, lygo_errors.Prefix(ErrorMissingWidgetSelector, SelectorThemiflyWidgetBlogPrevNextHere)
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	STATIC
// ---------------------------------------------------------------------------------------------------------------------

func loadMeta(file string) (meta *ThemiflyPostMeta) {
	text := readFile(file)
	if len(text) > 0 {
		return getMeta(text)
	}
	return getMeta(&ThemiflyPostMeta{})
}

func getMeta(i interface{}) (meta *ThemiflyPostMeta) {
	value := lygo_conv.ToString(i)
	_ = lygo_json.Read(value, &meta)
	if nil != meta {
		meta.BlogPagePrev = "#"
		meta.BlogPageNext = "#"
		if len(meta.AuthorImage) == 0 {
			meta.AuthorImage = ImageAuthor
		}
		if len(meta.Image) == 0 {
			meta.Image = ImagePost
		}
		if len(meta.Lang) == 0 {
			meta.Lang = "it"
		}
		if len(meta.Date) == 0 {
			meta.Date = lygo_date.FormatDate(time.Now(), lygo_date.DatePatterns[0])
		}
		// format date
		date, err := lygo_date.ParseAny(meta.Date)
		if nil == err {
			meta.Date = lygo_date.FormatDateLocale(date, meta.Lang)
		}
	}
	return
}

func blogPageId(count, postPerPage int) int {
	total := float64(count + 1)
	max := float64(postPerPage)
	pages := math.Ceil(total / max)

	return int(pages)
}

// update json data file with latest meta post info
func updateData(data *ThemiflyBloggerData, meta *ThemiflyPostMeta) {
	keywords := append(make([]string, 0), lygo_strings.Split(meta.Keywords, ",")...)

	// update data withs meta info
	if len(meta.Tags) > 0 {
		for _, tag := range meta.Tags {
			if nil == data.TagsAll {
				data.TagsAll = make(map[string]int)
			}
			if v, b := data.TagsAll[tag]; b {
				data.TagsAll[tag] = v + 1
			} else {
				data.TagsAll[tag] = 1
			}
			// add tag to keywords
			keywords = lygo_array.AppendUnique(keywords, tag).([]string)
		}
	}
	if len(meta.Categories) > 0 {
		for _, cat := range meta.Categories {
			if nil == data.CategoriesAll {
				data.CategoriesAll = make(map[string]int)
			}
			if v, b := data.CategoriesAll[cat]; b {
				data.CategoriesAll[cat] = v + 1
			} else {
				data.CategoriesAll[cat] = 1
			}
			// add cat to keywords
			keywords = lygo_array.AppendUnique(keywords, cat).([]string)
		}
	}

	meta.Keywords = strings.Join(keywords, ",")

	// update meta with data to use in mustache template engine
	tagsAll := make([]map[string]interface{}, 0)
	for k, v := range data.TagsAll {
		tagsAll = append(tagsAll, map[string]interface{}{"key": k, "val": v})
	}
	meta.TagsAll = tagsAll
	catAll := make([]map[string]interface{}, 0)
	for k, v := range data.CategoriesAll {
		catAll = append(catAll, map[string]interface{}{"key": k, "val": v})
	}
	meta.CategoriesAll = catAll

	// set microdata
	// normalizeMeta(meta)
}

func updateMicrodata(meta *ThemiflyPostMeta, vfs commons.IVfs) {
	if nil == meta.MicroData {
		meta.MicroData = new(ThemiflyPostMicroMeta)
	}

	// migrate images
	uri, err := vfsSaveImage(vfs, meta.Image)
	if nil == err && len(uri) > 0 {
		meta.Image = uri
	}

	// og
	if len(meta.MicroData.OgTitle) == 0 {
		meta.MicroData.OgTitle = meta.Title
	}
	if len(meta.MicroData.OgDescription) == 0 {
		meta.MicroData.OgDescription = meta.Description
	}
	if len(meta.MicroData.OgType) == 0 {
		meta.MicroData.OgType = "website"
	}
	if len(meta.MicroData.OgUrl) == 0 {
		meta.MicroData.OgUrl = meta.BlogPageSingle
	}
	if len(meta.MicroData.OgImage) == 0 {
		meta.MicroData.OgImage = meta.Image
	}

	// twitter
	if len(meta.MicroData.TwitterCard) == 0 {
		meta.MicroData.TwitterCard = "summary"
	}
	if len(meta.MicroData.TwitterCreator) == 0 {
		meta.MicroData.TwitterCreator = meta.Title
	}
	if len(meta.MicroData.TwitterSite) == 0 {
		meta.MicroData.TwitterSite = "@angelogeminiani"
	}
	if len(meta.MicroData.TwitterImage) == 0 {
		meta.MicroData.TwitterImage = meta.Image
	}
}
